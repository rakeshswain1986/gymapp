export class PaymentDetailsModel 
{
    public PaymentID: number;
    public PlanName: string;
    public ServiceName: string;
    public PaymentAmount: number;
    public PaymentFromdt: Date;
    public PaymentTodt: Date;
    public NextRenwalDate: Date;
    public RecStatus: string;
    public MemberName: string;
    public MemberNo: string;
}