import { Component } from '@angular/core';
import { ServiceMasterModel } from './app.ServiceModel';
import { ServiceMasterService } from './Services/app.ServiceMaster.Service';
import { Router } from '@angular/router';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBarConfig, MatSnackBar } from '@angular/material';

@Component({
    templateUrl: './app.ServiceMaster.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css']
})

export class ServiceComponent {
    title = "Service Master";
    ServiceForms: ServiceMasterModel = new ServiceMasterModel();
    private _ServiceService;
    private responsedata: any;
    
    actionButtonLabel: string = 'Retry';
    action: boolean = false;
    setAutoHide: boolean = true;
    autoHide: number = 2000;
    verticalPosition: MatSnackBarVerticalPosition = 'top';
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';



    constructor(private _Route: Router,public snackBar: MatSnackBar,  private ServiceService: ServiceMasterService) {
        this._ServiceService = ServiceService;
    }
    output: any;
    onSubmit() {
      

        this._ServiceService.SaveService(this.ServiceForms).subscribe(
            response => 
            {
               
                this.output = response;
                if (this.output.StatusCode == "409") 
                {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Service Name Already Exists", this.action ? this.actionButtonLabel : undefined, config);
                   
                }
                else if (this.output.StatusCode == "200") 
                { 
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Saved Service Successfully", this.action ? this.actionButtonLabel : undefined, config);
                    this._Route.navigate(['/Service/All']);
                }
                else {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Something Went Wrong", this.action ? this.actionButtonLabel : undefined, config);
                   
                }
            }
        );



    }

}