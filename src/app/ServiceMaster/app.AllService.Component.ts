import { Component, OnInit, ViewChild } from '@angular/core';
import { ServiceMasterService } from './Services/app.ServiceMaster.Service';
import { ServiceMasterViewModel } from './Models/app.ServiceViewModel';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  templateUrl: './app.AllServiceComponent.html',
  styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css', '../Content/vendor/font-awesome/css/font-awesome.min.css']

})

export class AllServiceComponent implements OnInit {
  private _ServiceService;
  AllServiceList: ServiceMasterViewModel[];
  errorMessage: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['ServiceId', 'ServiceName', 'Status', 'CreatedDate', 'EditAction', 'DeleteAction'];
  dataSource: any;
  
  constructor(private location: Location, private _Route: Router, private ServiceService: ServiceMasterService) {
    this._ServiceService = ServiceService;
  }

  ngOnInit() {
    this._ServiceService.GetAllService().subscribe(
      AllService => {
        this.AllServiceList = AllService
        this.dataSource = new MatTableDataSource(this.AllServiceList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
       
      },
      error => this.errorMessage = <any>error
    );

  }
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  Delete(ServiceId): void {
    if (confirm("Are you sure to delete Service ?")) {
      this._ServiceService.DeleteService(ServiceId).subscribe
        (
        response => {
          if (response.StatusCode == "200") {
            alert('Deleted Service Successfully');
            location.reload();
          }
          else {
            alert('Something Went Wrong');
            this._Route.navigate(['/Service/All']);
          }
        }
        )
    }
  }

}