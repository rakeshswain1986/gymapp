import { Component, OnInit } from '@angular/core';
import { ServiceMasterService } from './Services/app.ServiceMaster.Service';
import { ServiceMasterModel } from './app.ServiceModel';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: './app.EditServiceComponent.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css']
})

export class EditServiceComponent implements OnInit {
    title = "Edit Service Master";
    ServiceForms: ServiceMasterModel = new ServiceMasterModel();
    private _ServiceService;
    private responsedata: any;
    private ServiceID: string;
    errorMessage: any;

    constructor(private _Route: Router,private _routeParams: ActivatedRoute, private ServiceService: ServiceMasterService) {
        this._ServiceService = ServiceService;
    }

    ngOnInit() 
    {
        this.ServiceID = this._routeParams.snapshot.params['ServiceId'];
        if (this.ServiceID != null) 
        {
            var data = this._ServiceService.GetServiceById(this.ServiceID).subscribe(
                Service => {
                    this.ServiceForms.ServiceId = Service.SM_Id;
                    this.ServiceForms.ServiceName = Service.SM_Name;
                    this.ServiceForms.Status = Service.SM_IsActive;
                },
                error => this.errorMessage = <any>error
            );
        }
    }


    onSubmit() 
    {
   

        this._ServiceService.UpdateService(this.ServiceForms)
        .subscribe(response => 
        {
            if(response.StatusCode == "200")
            {
                alert('Updated Service Successfully');
                this._Route.navigate(['/Service/All']);
            }
        })
    }

}