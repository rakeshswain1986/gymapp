import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { MemberRegistrationModel } from '../Models/app.memberRegistrationModel';
import { MemberRegistrationGridModel } from '../Models/app.MemberRegistrationGridModel';
import { PaginationService } from '../../Shared/PaginationService';
import { environment } from "src/app/Shared/environment";
import { DropdownModel } from '../../Models/app.DropdownModel';
import { ActiveSubscriptionModel } from '../../Subscription/Models/app.ActiveSubscriptionModel';

@Injectable({
    providedIn: 'root'
})

export class MemberRegistrationService {
    private data: any;
    private apiUrl = environment.apiEndpoint + "/RegisterMember/";

    token: any;
    username: any;

    constructor(private http: HttpClient, private paginationService: PaginationService) {
        this.data = JSON.parse(localStorage.getItem('currentUser'));
        this.token = this.data.token;
        this.username = this.data.username;
    }

    getAll<T>() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        let Url = environment.apiEndpoint + "/RegisterMember";
        const mergedUrl = `${Url}` +
            `?page=${this.paginationService.page}&pageCount=${this.paginationService.pageCount}`;

        return this.http.get<T>(mergedUrl, { headers: headers, observe: 'response' }).pipe(
            catchError(this.handleError)
        );
    }

    // Save Member
    public SaveMember(memberModel: MemberRegistrationModel) 
    {
        var SaveUrl = environment.apiEndpoint +"/RegisterMember";
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.post<any>(SaveUrl, memberModel, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }


    // Update Member
    public UpdateMember(memberModel: MemberRegistrationModel) {
        var updateUrl = environment.apiEndpoint +"/RegisterMember/" + memberModel.MemberId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.put<any>(updateUrl, memberModel, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    // Get All Member
    public GetAllMember() 
    {
        var getUrl = environment.apiEndpoint +"/RegisterMember";
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<MemberRegistrationGridModel[]>(this.apiUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }
    // Get Member by MemberID

    public GetMemberById(MemberId) {
        console.log(MemberId); 
        var editUrl = environment.apiEndpoint +"/RegisterMember"+ '/' + MemberId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<MemberRegistrationModel>(editUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    public DeleteMember(MemberId) {
        var deleteUrl = environment.apiEndpoint +"/RegisterMember"+ '/' + MemberId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.delete<any>(deleteUrl, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    public GetAllActiveServiceList() {
        var url = environment.apiEndpoint + "/ServiceDropdown/";
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<DropdownModel[]>(url, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)

        );
    }

    public GetAllActiveSubscriptions(ServiceId) {
        var url = environment.apiEndpoint + "/AllActiveSubscriptionMaster" + '/' + ServiceId;;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<ActiveSubscriptionModel[]>(url, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    public GetAmount(planID: string, ServiceId: string) {
        var url = environment.apiEndpoint + "/GetTotalAmount/";
        let AmountRequest = { "SubscriptionId": planID, "ServiceId": ServiceId };

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.post<string>(url, AmountRequest, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }


    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };
}