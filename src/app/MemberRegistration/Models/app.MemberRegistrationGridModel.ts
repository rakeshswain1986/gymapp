export class MemberRegistrationGridModel {
    public MemberId: number;
    public MemberNo: string;
    public MemberName: string;
    public MemberMName: number;
    public Dob: Date;
    public Contactno: string;
    public EmailId: string;
    public PlanName: string;
    public ServiceName: string;
    public ServiceID: string;
    public JoiningDate: string;
    public Amount: number;
}