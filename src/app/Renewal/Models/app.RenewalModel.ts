export class RenewalModel 
{
    public MemberName: string;
    public MemberNo: string;
    public MemberId : number;
    public PlanID : number;
    public ServiceID : string;
    public SearchMemberNo: string;
    public Amount : number;
    public NewDate : string;  
    public NextRenwalDate : string;  
}