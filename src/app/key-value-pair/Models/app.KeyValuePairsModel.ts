export class KeyValuePairModel 
{
    public KeyId: number = 0;
    public KeyType: string = "";
    public KeyCode: string = "";
    public KeyParent:number = 0;
}