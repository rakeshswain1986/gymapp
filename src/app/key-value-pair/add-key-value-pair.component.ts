import { Component } from '@angular/core';
import { KeyValuePairModel } from './Models/app.KeyValuePairsModel';
import { KeyValuePairService } from './Services/app.key-value-pair.service';
import { Router } from '@angular/router';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBarConfig, MatSnackBar } from '@angular/material';

@Component({
    templateUrl: './add-key-value-pair.component.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
    '../Content/vendor/metisMenu/metisMenu.min.css',
    '../Content/dist/css/sb-admin-2.css',
    '../Content/vendor/font-awesome/css/font-awesome.min.css']
})

export class AddKeyValuePairComponent {
    title = "Service Master";
    KeyValuePairModel: KeyValuePairModel = new KeyValuePairModel();
    private _KeyValuePairService;
    private responsedata: any;
    
    actionButtonLabel: string = 'Retry';
    action: boolean = false;
    setAutoHide: boolean = true;
    autoHide: number = 2000;
    verticalPosition: MatSnackBarVerticalPosition = 'top';
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';



    constructor(private _Route: Router,public snackBar: MatSnackBar,  private KeyValuePairService: KeyValuePairService) {
        this._KeyValuePairService = KeyValuePairService;
    }
    output: any;
    onSubmit() {
      

        this._KeyValuePairService.Save(this.KeyValuePairModel).subscribe(
            response => 
            {
               
                this.output = response;
                if (this.output.StatusCode == "409") 
                {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Service Name Already Exists", this.action ? this.actionButtonLabel : undefined, config);
                   
                }
                else if (this.output.StatusCode == "200") 
                { 
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Saved Service Successfully", this.action ? this.actionButtonLabel : undefined, config);
                    this._Route.navigate(['/KeyValuePair/All']);
                }
                else {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Something Went Wrong", this.action ? this.actionButtonLabel : undefined, config);
                   
                }
            }
        );



    }

}