import { Component, OnInit, ViewChild } from '@angular/core';
import { KeyValuePairService } from  './Services/app.key-value-pair.service';
import { KeyValuePairModel } from './Models/app.KeyValuePairsModel';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  templateUrl: './key-value-pair.component.html',
  styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css', 
              '../Content/vendor/font-awesome/css/font-awesome.min.css']

})

export class KeyValuePairComponent implements OnInit {
  private _KeyValuePairService;
  KeyValueList: KeyValuePairModel[];
  errorMessage: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['KeyId','KeyType','KeyCode', 'KeyName', 'Status', 'CreatedDate', 'EditAction', 'DeleteAction'];
  dataSource: any;
  
  constructor(private location: Location, private _Route: Router, private KeyValuePairService: KeyValuePairService) {
    this._KeyValuePairService = KeyValuePairService;
  }

  ngOnInit() {
    this._KeyValuePairService.GetAll().subscribe(
      AllService => {
        this.KeyValueList = AllService
        this.dataSource = new MatTableDataSource(this.KeyValueList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
       
      },
      error => this.errorMessage = <any>error
    );

  }
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  Delete(KeyId): void {
    if (confirm("Are you sure to delete Service ?")) {
      this._KeyValuePairService.Delete(KeyId).subscribe
        (
        response => {
          if (response.StatusCode == "200") {
            alert('Deleted Service Successfully');
            location.reload();
          }
          else {
            alert('Something Went Wrong');
            this._Route.navigate(['/Service/All']);
          }
        }
        )
    }
  }

}