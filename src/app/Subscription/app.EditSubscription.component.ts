import { Component, OnInit } from '@angular/core';
import { PeriodService } from '../PeriodMaster/Services/app.Period.Service';
import { ServiceMasterService } from '../ServiceMaster/Services/app.ServiceMaster.Service';
import { PeriodModel } from '../PeriodMaster/Models/app.PeriodModel';
import { DropdownModel } from '../Models/app.DropdownModel';
import { SubscriptionService } from './Services/app.subscription.service';
import { SubscriptionMasterModel } from './Models/app.SubscriptionMasterModel';
import { Router, ActivatedRoute } from '@angular/router'

@Component({
    templateUrl: './app.EditSubscription.component.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
        '../Content/vendor/metisMenu/metisMenu.min.css',
        '../Content/dist/css/sb-admin-2.css',
        '../Content/vendor/font-awesome/css/font-awesome.min.css'
    ]
})


export class EditSubscriptionComponent implements OnInit {
    private _periodService;
    private _ServiceMasterService;
    private _planService;
    PeriodList: PeriodModel[];
    AllActiveServiceList: DropdownModel[];
    errorMessage: any;
    planModel: SubscriptionMasterModel = new SubscriptionMasterModel();
    SubscriptionID: any;
    output: any;


    constructor(private _Route: Router, private _routeParams: ActivatedRoute, private periodService: PeriodService, private ServiceMasterService: ServiceMasterService, private planService: SubscriptionService) {
        this._periodService = periodService;
        this._ServiceMasterService = ServiceMasterService;
        this._planService = planService;
    }

    ngOnInit(): void {

        this.SubscriptionID = this._routeParams.snapshot.params['SubscriptionID'];
       


        // GetAllPeriod
        this._periodService.GetAllPeriod().subscribe(
            allPeriod => {
                this.PeriodList = allPeriod
            
            },
            error => this.errorMessage = <any>error
        );

        // GetAllService

        this._ServiceMasterService.GetAllActiveServiceList().subscribe(
            allActiveService => {
                this.AllActiveServiceList = allActiveService
            },
            error => this.errorMessage = <any>error
        );

        // GetSubscriptionBySubscriptionID

        this._planService.GetSubscriptionBySubscriptionID(this.SubscriptionID).subscribe(
            plan => {
                this.planModel = plan
    
            },
            error => this.errorMessage = <any>error
        );



    }


    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    }


    onSubmit() 
    {
        this._planService.UpdateSubscription(this.planModel).subscribe(
            response => {
                this.output = response
                if (this.output.StatusCode == "409") {
                    alert('Subscription Already Exists');
                }
                else if (this.output.StatusCode == "200") {
                    alert('Subscription Saved Successfully');
                    this._Route.navigate(['/Subscription/All']);
                }
                else {
                    alert('Something Went Wrong');
                }
            });
    }
}