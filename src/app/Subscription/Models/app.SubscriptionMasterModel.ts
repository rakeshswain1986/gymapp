export class SubscriptionMasterModel{

    public SubscriptionID : number;
    public SubscriptionName : string;
    public SubscriptionAmount : number;
    public ServiceTax : string;
    public ServiceID : number;
    public PeriodID : number;
}