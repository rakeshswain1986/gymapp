export class SubscriptionMasterViewModel {

    public SubscriptionID: number;
    public SubscriptionName: string;
    public SubscriptionAmount: number;
    public ServicetaxAmount: string;
    public ServiceTax: string;
    public RecStatus: boolean;
    public ServiceID: number;
    public ServiceName: string;
    public PeriodID: number;
    public Text: string;
    public TotalAmount: number;
    public ServicetaxNo: string;
}