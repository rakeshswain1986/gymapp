import { Component, OnInit, ViewChild } from '@angular/core';
import { SubscriptionService } from './Services/app.subscription.service';
import { SubscriptionMasterViewModel } from './Models/app.SubscriptionMasterViewModel';
import { Router } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
    templateUrl: './app.Subscription.component.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
        '../Content/vendor/metisMenu/metisMenu.min.css',
        '../Content/dist/css/sb-admin-2.css',
        '../Content/vendor/font-awesome/css/font-awesome.min.css'
    ]
})

export class SubscriptionComponent implements OnInit {
    private _planService;
    SubscriptionList: SubscriptionMasterViewModel = new SubscriptionMasterViewModel();
    errorMessage: any;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    displayedColumns: string[] = ['SubscriptionID', 'SubscriptionName', 'ServiceName', 'Text', 'TotalAmount', 'RecStatus', 'EditAction', 'DeleteAction'];
    dataSource: any;
    constructor(private _Route: Router, private planService: SubscriptionService) {
        this._planService = planService;
    }
    ngOnInit(): void {


        this._planService.GetAllSubscriptions().subscribe(
            allplan => {
                this.SubscriptionList = allplan
                this.dataSource = new MatTableDataSource(allplan);
                this.dataSource.sort = this.sort;
                this.dataSource.paginator = this.paginator;
            },
            error => this.errorMessage = <any>error
        );
    }


    Delete(SubscriptionID) {
        if (confirm("Are you sure to delete Subscription ?")) {
            this._planService.DeleteSubscription(SubscriptionID).subscribe
                (
                response => {
                    if (response.StatusCode == "200") {
                        alert('Deleted Subscription Successfully');
                        location.reload();
                    }
                    else {
                        alert('Something Went Wrong');
                        this._Route.navigate(['/Subscription/All']);
                    }
                }
                )
        }
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
}