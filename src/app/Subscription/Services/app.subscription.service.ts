import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { SubscriptionMasterModel } from '../Models/app.SubscriptionMasterModel';
import { SubscriptionMasterViewModel } from '../Models/app.SubscriptionMasterViewModel';
import { ActiveSubscriptionModel } from '../Models/app.ActiveSubscriptionModel';
import { environment } from 'src/app/Shared/environment';


@Injectable({
    providedIn: 'root'
})

export class SubscriptionService 
{
    private data: any;
    private apiUrl = environment.apiEndpoint + "/SubscriptionMaster/";
    token: any;
    username: any;


    constructor(private http: HttpClient) {
        this.data = JSON.parse(localStorage.getItem('AdminUser'));
        this.token = this.data.token;
    }

    // Save Subscription
    public SaveSubscription(planMasterModel: SubscriptionMasterModel) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.post<any>(this.apiUrl, planMasterModel, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );

    }

    public GetAmount(planID: string, ServiceId: string) 
    {
        var apiUrl = environment.apiEndpoint+"/GetTotalAmount/";
        let AmountRequest = { "SubscriptionId": planID,"ServiceId":ServiceId};
        
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.post<string>(apiUrl, AmountRequest, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }



    // Get All Subscriptions
    public GetAllSubscriptions() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<SubscriptionMasterViewModel[]>(this.apiUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    // Get All Subscriptions
    public GetAllActiveSubscriptions(ServiceId) {
        var apiUrl = environment.apiEndpoint +"/AllActiveSubscriptionMaster" + '/' + ServiceId;;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<ActiveSubscriptionModel[]>(apiUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }


    // Get All Subscriptions by SubscriptionId
    public GetSubscriptionBySubscriptionID(planId) {
        var editurl = this.apiUrl + planId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<SubscriptionMasterViewModel[]>(editurl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }


    // Update Subscription
    public UpdateSubscription(planMasterModel: SubscriptionMasterModel) {
        var updateurl = this.apiUrl + planMasterModel.SubscriptionID;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.put<any>(updateurl, planMasterModel, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    public DeleteSubscription(planId) {
        var deleteUrl = this.apiUrl + '/' + planId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.delete<any>(deleteUrl, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }
    public SearchMember(keyword:string) {
        var getUrl = environment.apiEndpoint + '/GetMemberNo/Search?key=' + keyword;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<any>(getUrl, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };

}