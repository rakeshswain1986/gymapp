import { Component, OnInit } from '@angular/core';
import { PeriodService } from '../PeriodMaster/Services/app.Period.Service';
import { ServiceMasterService } from '../ServiceMaster/Services/app.ServiceMaster.Service';
import { PeriodModel } from '../PeriodMaster/Models/app.PeriodModel';
import { SubscriptionMasterModel } from './Models/app.SubscriptionMasterModel';
import { DropdownModel } from '../Models/app.DropdownModel';
import { SubscriptionService } from './Services/app.subscription.service';
import { Router } from '@angular/router';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import 'rxjs/add/operator/debounceTime';
import { MemberRegistrationModel } from '../MemberRegistration/Models/app.memberRegistrationModel';

@Component({
    templateUrl: './app.AddSubsctiption.component.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
        '../Content/vendor/metisMenu/metisMenu.min.css',
        '../Content/dist/css/sb-admin-2.css',
        '../Content/vendor/font-awesome/css/font-awesome.min.css',
        './app.subscription.component.css'
    ]
})

export class SubscriptionMasterComponent implements OnInit {
    private _periodService;
    private _ServiceMasterService;
    private _subscriptionService;
    memberCtrl: FormControl;
    filteredMember: Observable<any[]>;
    members: MemberRegistrationModel[]
    PeriodList: PeriodModel[];
    AllActiveServiceList: DropdownModel[];
    errorMessage: any;
    subsriptionModel: SubscriptionMasterModel = new SubscriptionMasterModel();
    title = 'Add Subscription';
    output: any;

    constructor(private _Route: Router,private periodService: PeriodService,
        private ServiceMasterService: ServiceMasterService,
        private subscriptionService: SubscriptionService
    ) {
        this._periodService = periodService;
        this._ServiceMasterService = ServiceMasterService;
        this._subscriptionService = subscriptionService;
        this.memberCtrl = new FormControl();
        this.memberCtrl.valueChanges
        .debounceTime(400) 
        .subscribe(data => {
            this._subscriptionService.SearchMember(data).subscribe(response =>{
                this.members = response
            })
        })
    }

    ngOnInit(): void {
        this._periodService.GetAllPeriod().subscribe(
            allPeriod => {
                this.PeriodList = allPeriod
            },
            error => this.errorMessage = <any>error
        );

        this._ServiceMasterService.GetAllActiveServiceList().subscribe(
            allActiveService => {
                this.AllActiveServiceList = allActiveService
            },
            error => this.errorMessage = <any>error
        );
    }
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    }
    displayFn(member?: any): string | undefined {
        debugger;
        return member ? member.MemberName : undefined;
      }
    onSubmit() {
     

        this._subscriptionService.SaveSubscription(this.subsriptionModel).subscribe(
            response => {
            this.output = response
            if (this.output.StatusCode == "409") {
                alert('Subscription Already Exists');
            }
            else if (this.output.StatusCode == "200") {
                alert('Subscription Saved Successfully');
                this._Route.navigate(['/Subscription/All']);
            }
            else {
                alert('Something Went Wrong');
            }
        });
    }
}